// password-partition-backend/server.js
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const authentication = require("./utils/json-web-token");
const mongoose = require("mongoose");

mongoose.connect("mongodb://127.0.0.1:27017/task1", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// importing files
const user = require("./controller/user");
const student = require("./controller/student");
const skill = require("./controller/skills");

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use("/api/user", user);
app.use("/api/student", authentication.verify, student);
app.use("/api/skill", authentication.verify, skill);

app.listen(3002, () => {
  console.log("Server is running on port 3002");
});

module.exports = app;
