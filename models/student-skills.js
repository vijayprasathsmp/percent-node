const mongoose = require("mongoose");

const StudentSkills = new mongoose.Schema(
  {
    skill: { type: mongoose.Schema.ObjectId, index: true },
    student_id: {
      type: mongoose.Schema.ObjectId,
      index: true,
      required: true,
    },
    percentage: { type: Number },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StudentSkills", StudentSkills);
