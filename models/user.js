const mongoose = require("mongoose");

const User = new mongoose.Schema(
  {
    user_name: { type: String, index: true },
    password: { type: String },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("user", User);
