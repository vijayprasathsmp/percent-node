const mongoose = require("mongoose");

const Skill = new mongoose.Schema(
  {
    name: { type: String },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("skill", Skill);
