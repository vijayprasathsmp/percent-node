const mongoose = require("mongoose");

const Student = new mongoose.Schema(
  {
    name: { type: String },
    role: { type: String },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("student", Student);
