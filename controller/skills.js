const express = require("express");
const router = express.Router();
const Skill = require("../models/skills");
const Student = require("../models/students");
const StudentSkill = require("../models/student-skills");

router.post("/", async (req, res) => {
  try {
    const { name } = req.body;
    const skill = new Skill({ name });
    await skill.save();
    res.status(200).send({
      status: true,
      message: "skill created successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.get("/", async (req, res) => {
  try {
    const data = await Skill.find();

    res.status(200).send({
      status: true,
      message: "skill list successfully",
      data,
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.put("/", async (req, res) => {
  try {
    const { id, name } = req.body;
    await Skill.findByIdAndUpdate(id, { name });

    res.status(200).send({
      status: true,
      message: "skill updated successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.put("/", async (req, res) => {
  try {
    const { id } = req.body;
    await Skill.findByIdAndRemove(id);

    res.status(200).send({
      status: true,
      message: "skill deleted successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

// dashboard
router.get("/dashboard", async (req, res) => {
  try {
    const data = await Skill.aggregate([
      {
        $lookup: {
          from: StudentSkill.collection.name,
          localField: "_id",
          foreignField: "skill",
          as: "matchedData",
        },
      },
      {
        $unwind: {
          path: "$matchedData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: "$_id",
          name: { $first: "$name" },
          total_skill_value: { $sum: "$matchedData.percentage" },
          matchedDataCount: { $sum: 100 },
        },
      },
      {
        $addFields: {
          percentage: {
            $cond: {
              if: {
                $and: [
                  { $ne: ["$matchedDataCount", 0] },
                  { $ne: ["$total_skill_value", 0] },
                ],
              },

              then: {
                $multiply: [
                  {
                    $divide: ["$total_skill_value", "$matchedDataCount"],
                  },
                  100,
                ],
              },
              else: 0,
            },
          },
        },
      },
      {
        $sort: {
          _id: -1,
        },
      },
      {
        $project: {
          _id: 1,
          name: 1,
          percentage: 1,
        },
      },
    ]);

    res.status(200).send({
      status: true,
      message: "list successfully",
      data,
    });
  } catch (error) {
    console.log(error);
    res.status(404).send(error);
  }
});

router.get("/dashboard1", async (req, res) => {
  try {
    const studentCount = await Student.find();

    const data = await Skill.aggregate([
      {
        $lookup: {
          from: StudentSkill.collection.name,
          localField: "_id",
          foreignField: "skill",
          as: "matchedData",
        },
      },
      {
        $unwind: {
          path: "$matchedData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: "$_id",
          name: { $first: "$name" },
          total_skill_value: { $sum: "$matchedData.percentage" },
          matchedDataCount: { $first: studentCount.length * 100 },
        },
      },
      {
        $addFields: {
          percentage: {
            $cond: {
              if: {
                $and: [
                  { $ne: ["$matchedDataCount", 0] },
                  { $ne: ["$total_skill_value", 0] },
                ],
              },

              then: {
                $multiply: [
                  {
                    $divide: ["$total_skill_value", "$matchedDataCount"],
                  },
                  100,
                ],
              },
              else: 0,
            },
          },
        },
      },
      {
        $sort: {
          _id: -1,
        },
      },
      {
        $project: {
          _id: 1,
          name: 1,
          percentage: 1,
          matchedDataCount: 1,
        },
      },
    ]);

    res.status(200).send({
      status: true,
      message: "list successfully",
      data,
    });
  } catch (error) {
    console.log(error);
    res.status(404).send(error);
  }
});

module.exports = router;
