const express = require("express");
const router = express.Router();
const Student = require("../models/students");
const StudentSkill = require("../models/student-skills");
const SkillModel = require("../models/skills");

router.post("/", async (req, res) => {
  try {
    const { name, skills, role } = req.body;
    const student = new Student({ name, role });
    await student.save().then(async (data) => {
      if (skills) {
        for (let i = 0; i < skills.length; i++) {
          const studentSkill = new StudentSkill({
            skill: skills[i].skill,
            student_id: data._id,
            percentage: skills[i].percentage,
          });
          await studentSkill.save();
        }
      }
    });
    res.status(200).send({
      status: true,
      message: "student created successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.get("/", async (req, res) => {
  try {
    const data = await Student.aggregate([
      {
        $lookup: {
          from: StudentSkill.collection.name,
          localField: "_id",
          foreignField: "student_id",
          as: "skills",
        },
      },
      {
        $unwind: {
          path: "$skills",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: SkillModel.collection.name,
          localField: "skills.skill",
          foreignField: "_id",
          as: "skills.skill",
        },
      },
      {
        $sort: {
          _id: -1,
        },
      },
      {
        $group: {
          _id: "$_id",
          name: { $first: "$name" },
          role: { $first: "$role" },

          skills: { $push: "$skills" },
        },
      },
    ]);

    res.status(200).send({
      status: true,
      message: "student list successfully",
      data,
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.put("/", async (req, res) => {
  try {
    const { id, name, skills, role } = req.body;
    await Student.findByIdAndUpdate(id, { name, role }).then(async (data) => {
      await StudentSkill.deleteMany({ student_id: id });
      if (skills) {
        for (let i = 0; i < skills.length; i++) {
          const studentSkill = new StudentSkill({
            skill: skills[i].skill,
            student_id: id,
            percentage: skills[i].percentage,
          });
          await studentSkill.save();
        }
      }
    });

    res.status(200).send({
      status: true,
      message: "student updated successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.delete("/", async (req, res) => {
  try {
    const { id } = req.query;
    await Student.findByIdAndRemove(id);
    await StudentSkill.deleteMany({ student_id: id });

    res.status(200).send({
      status: true,
      message: "student deleted successfully",
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

module.exports = router;
